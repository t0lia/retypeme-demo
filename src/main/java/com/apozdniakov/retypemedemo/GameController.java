package com.apozdniakov.retypemedemo;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class GameController {

    private final ConcurrentHashMap<String, Map<String, RacerStat>> store =
        new ConcurrentHashMap<>();

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Session greeting(Session message) {
        String session = message.getId();

        Map<String, RacerStat> racerStats = store.getOrDefault(session, new ConcurrentHashMap<>());

        racerStats.put(message.getUser().getUsername(), message.getUser());
        if (store.size() < 20) {
            store.put(session, racerStats);
        } else {
            store.clear();
        }

        Session result = new Session(message.getId(), message.getUser(),
            new ArrayList<>(store.get(message.getId()).values()));
        return result;
    }

}

package com.apozdniakov.retypemedemo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RacerStat {
    private String username;
    private int progress;

}

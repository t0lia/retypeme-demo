package com.apozdniakov.retypemedemo;


import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Session {
    private String id;
    private RacerStat user;
    private List<RacerStat> racers;
}

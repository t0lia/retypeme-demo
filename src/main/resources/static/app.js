let text = "No one had a more attentive audience than old Ham Gamgee, commonly known as the Gaffer.";

let uid = '9f5c7a3e-2d84-4b6c-baeb-8d1f9e0a731d';
let count = 0;

// filled with random name from racerNames
let username = "";
$(function () {
    document.getElementById("text").textContent = text;
    document.getElementById("snd").textContent = text;
    document.getElementById("fst").textContent = '';
});
window.onload = function () {
    username = localStorage.getItem('username');
    if (username) {
        document.getElementById('name').style.display = 'none';
        document.getElementById('nameContainer').innerText = username;
        document.getElementById('running').style.display = 'block';
        document.getElementById('logoffButton').style.display = 'block';
    } else {
        document.getElementById('running').style.display = 'none';
        document.getElementById('logoffButton').style.display = 'none';
    }
};

function saveSession() {
    uid = document.getElementById('sessionInput').value;
    document.getElementById("text").textContent = text;
    document.getElementById("snd").textContent = text;
    document.getElementById("fst").textContent = '';
    count = 0;

    const usersContainer = document.getElementById('users-container');
    usersContainer.innerHTML = '';
}
function saveUsername() {
    // Get the value from the input field
    username = document.getElementById('usernameInput').value;

    // Check if the username is not empty
    if (username.trim() !== '') {
        // Save the username to local storage
        localStorage.setItem('username', username);
        document.getElementById('name').style.display = 'none';
        document.getElementById('running').style.display = 'block';
        document.getElementById('nameContainer').innerText = username;
        document.getElementById('logoffButton').style.display = 'block';
    } else {
    }
}
function logoff() {
    // Clear the username from local storage
    localStorage.removeItem('username');

    document.getElementById('name').style.display = 'block';
    document.getElementById('running').style.display = 'none';
    document.getElementById('nameContainer').innerText = '';
    document.getElementById('logoffButton').style.display = 'none';
}

document.addEventListener('keypress', (event) => {
    const char = String.fromCharCode(event.charCode);

    if (/^[\x20-\x7E]$/.test(char)) {
        let cnt = document.getElementById("fst").textContent + char;
        if (text.startsWith(cnt)) {
            count++;
            document.getElementById("fst").textContent = text.substring(0, count);
            document.getElementById("snd").textContent = text.substring(count);

            // rounded progress
            let body = JSON.stringify({
                'id': uid,
                'user': {
                    'progress': Math.round(count * 100 / text.length),
                    'username': username,
                },
                'racers': [],
            });
            syncStat(body);
        }
    }
});

const stompClient = new StompJs.Client({
    brokerURL: 'ws://65.109.132.132:8081/gs-guide-websocket'
    // brokerURL: 'ws://localhost:8080/gs-guide-websocket'
});

stompClient.onConnect = (frame) => {
    setConnected(true);
    console.log('Connected: ' + frame);
    stompClient.subscribe('/topic/greetings', (greeting) => {
        let jsonData = JSON.parse(greeting.body);
        console.log(jsonData);
        console.log(greeting.body);


        const usersContainer = document.getElementById('users-container');
        usersContainer.innerHTML = '';
        const racers = jsonData.racers;

        // Displaying users and their progress
        racers.forEach(racer => {
            const userContainer = document.createElement('div');
            userContainer.classList.add('user-container');

            userContainer.innerHTML = `
                    <p><strong>Username:</strong> ${racer.username}</p>
                    <div class="progress-bar">
                        <div class="progress" style="width: ${racer.progress}%">${racer.progress}%</div>
                    </div>
                `;

            usersContainer.appendChild(userContainer);
        });
    });
};

stompClient.onWebSocketError = (error) => {
    console.error('Error with websocket', error);
};

stompClient.onStompError = (frame) => {
    console.error('Broker reported error: ' + frame.headers['message']);
    console.error('Additional details: ' + frame.body);
};

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    } else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    stompClient.activate();
}

function disconnect() {
    stompClient.deactivate();
    setConnected(false);
    console.log("Disconnected");
}

function syncStat(body) {
    stompClient.publish({
        destination: "/app/hello",
        body: body
    });
}

$(function () {
    connect();
});